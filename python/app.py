from fastapi import FastAPI
from pymongo import MongoClient
from pydantic import BaseModel
from typing import Optional
from asyncio import sleep

class Poll(BaseModel):
  topic: str
  vote: Optional[int] = 1

app = FastAPI()

client = MongoClient("mongodb://root:password@localhost:27017/")

database = client["polling"]

collection = database["votes"]

@app.post('/polls')
async def store(poll: Poll):
  await vote(poll.dict())
  return {"success": True}

# @app.get('/polls')
# async def index():
#   return get_votes()

async def vote(poll):
  await routine()
  # collection.insert_one(poll)

# def get_votes():
#   votes = [{"topic":vote['topic']} for vote in collection.find()]

#   return votes

async def routine():
  for x in range(333333):
    square = x * x