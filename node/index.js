const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv').config()
const cors = require('cors')
const polls = require('./routes/index.js')

const port = process.env.PORT || 3000

const app = express();

mongoose.connect(
  'mongodb://localhost/polling',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)
.then(() => console.log('Connected to mongoDb'))
.catch(err => console.log('An error occured while connecting to mongoDb:', err))

app.use(cors())
app.use(express.json())


app.get('/polls', (req, res) => {
  for (let i = 0; i < 333333; i++) {
    const square = i * i;
  }
});
// app.use('/polls', polls)

app.get('*', (req, res) => {
  res.status(404)

  return res.send('Not found')
})

app.listen(port, () => {
  console.log(`Listening on port ${port}...`)
})