const express = require("express")

let router = express.Router()

router.use(express.json());
router.use(express.urlencoded({ extended: false }));

router.get('/', (req, res) => {
  console.log('incomming')
  res.send({
    message: 'ok',
    success: true
  })
})

module.exports = router;